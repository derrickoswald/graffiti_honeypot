# Graffiti Honeypot

A low cost monitor for remote graffiti prone areas that would notify authorities when
graffiti activity is taking place and optionally photograph perpetrators in the act.

![Graffiti](img/graffiti.jpg "Graffiti")

## Background

This idea has been kicking around since 2013, as a proof of concept design exercise,
with nothing concrete to show for it except a document containing power and cost budgets.
At the time, the power budget calculation indicated that a device could be built
that could last years on a single pair of AA batteries, but the economic budget came in at
close to $200, which belied the low cost "moniker", so it was put on the back burner.

After a Covid hiccough, [Make Zurich 2020](https://now.makezurich.ch/)
was rescheduled and took place October 23-31, 2020.
It brings together the local hacker/maker community, and the city administration
to explore new ways of solving the problems of Zurich with the help of open networks and civic tech.
One of the major sponsors is the [Open Network Infrastructure Association](https://opennetworkinfrastructure.org/)
which brings to the table extensive experience in the
[Internet of Things (IoT)](https://en.wikipedia.org/wiki/Internet_of_things) space,
and in particular
[The Things Network](https://www.thethingsnetwork.org/)
which is enabled by [LoRaWan](https://en.wikipedia.org/wiki/Lorawan).

Make Zurich invited "open challenges" and given the Internet of Things focus,
this project [was submitted](https://github.com/make-zurich/open-challenge-ideas-2020/issues/1)
as an alternative challenge.

Make Zurich participants received a [kit of parts](construction/makezurich_kit.md) which
included a LoRaWan capable, Arduino compatible development board, among _very_ many other pieces. 

## Overview

In the scope of this Make Zurich challenge, a simple circuit was created to enable motion detection
that then wakes a microprocessor, which can then perform monitoring of a graffiti prone area.

![Graffiti Honeypot Concept](img/graffiti%20honeypot%20concept.svg "Graffiti Honeypot Concept")

The initial circuit concept is shown below.

![Graffiti Honeypot Block Diagram](img/graffiti%20honeypot%20block%20diagram.svg "Graffiti Honeypot Block Diagram")

## Construction

Over the following sequential construction steps, participants built this circuit.

![Graffiti Honeypot Sample Breadboard](img/sample_breadboard.jpg "Graffiti Honeypot Sample Breadboard")

From there, hacking, experiments and discovery of LoRaWan/The Things Network is taking place.
The current version of the circuit diagram is shown below and the steps are provided via links.

![Graffiti Honeypot Circuit Diagram](img/circuit%20diagram.svg "Graffiti Honeypot Circuit Diagram")

1. [Passive Infra-Red (PIR) Sensor Circuit](construction/Step%201.md)
2. [Load Switch Circuit](construction/Step%202.md)    
3. [Hysteresis Circuit](construction/Step%203.md)
4. [Sense Circuit](construction/Step%204.md)
5. [SD Card Circuit](construction/Step%205.md)
6. [Camera Module Circuit](construction/Step%206.md)

## Exploration

Some possible next steps:

- get the real time from the Things Network
- add a battery charge sensor
- 

