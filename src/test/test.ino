/*
  Graffiti Honeypot Test Program
  Turns on an LED on for one second, then off for one second if the PIR sensor is off, repeatedly.

  modified 17 October 2020 (from Blink)
  by Derrick Oswald

*/

int PIR = 12;     // motion sensor sense circuit connected to pin 12
int state = 0;    // variable to store the read value

// the setup function runs once when you press reset or power the board
void setup() {

  pinMode(LED_BUILTIN, OUTPUT); // set digital pin LED_BUILTIN as an output
  pinMode(PIR, INPUT);          // sets the digital pin 12 as input
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);    // turn the LED on
  delay(1000);                        // wait for a second
  state = digitalRead(PIR);           // read the motion sensor state
  if (state == HIGH)                  // if the sensor is off
  {
      digitalWrite(LED_BUILTIN, LOW); // turn the LED off
  }
  delay(1000);                        // wait for a second
}
