function Decoder(bytes, port) {

    function digits(x, digits) {
        var hi = 1 << (digits * 4);
        var num = (hi | (x & (hi - 1)));
        return num.toString(16).substring(1);
    }

    function toUUID(shorts) {
        return (digits(shorts[0], 4) + digits(shorts[1], 4) + "-" +
            digits(shorts[2], 4) + "-" +
            digits(shorts[3], 4) + "-" +
            digits(shorts[4], 4) + "-" +
            digits(shorts[5], 4) +  digits(shorts[6], 4) + digits(shorts[7], 4));
    }


    function toShort(b0, b1)
    {
        return ((b0 << 8) |  b1);
    }

    var shorts = [
    toShort(bytes[0],bytes[1]),
    toShort(bytes[2],bytes[3]),
    toShort(bytes[4],bytes[5]),
    toShort(bytes[6],bytes[7]),
    toShort(bytes[8],bytes[9]),
    toShort(bytes[10],bytes[11]),
    toShort(bytes[12],bytes[13]),
    toShort(bytes[14],bytes[15])
    ];

    var time =
        (bytes[16] << 24)
    | (bytes[17] << 16)
    | (bytes[18] << 8)
    |  bytes[19];

    var motion = bytes[20] !== 0;
  
    var decoded = {
        "event": toUUID (shorts),
        "time": time,
        "motion": motion
    };

    return decoded;
}

