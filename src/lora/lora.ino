/*  Arduino code for Graffiti Honeypot.
 *
 *  Performs an ABB join for TheThingNetwork LoRaWAN network,
 *  then emits an event based for the sensor activation.
 *
 *  Uncomment one of the region defined below to select the
 *  proper radio setup.
 *    
 *  EU868/IN865 have duty cycle restrictions. For debugging it makes sense
 *  to disable those via setDutyCycle(false);
 *    
 *  For an external antenna one should set the proper antenna gain
 *  (default is 2.0) via setAntennaGain().
 *    
 */

#include "LoRaWAN.h"
#include <SD.h>

// #define REGION_AS923_920_923 /* Japan, Malaysia, Singapore */
// #define REGION_AS923_923_925 /* Brunei, Cambodia, Hong Kong, Indonesia, Laos, Taiwan, Thailand, Vietnam */
// #define REGION_AU915
#define REGION_EU868
// #define REGION_IN865
// #define REGION_KR920
// #define REGION_US915

#include "secrets.h"
// is a file in the same directory as this file containing secret keys of the form:
//
// #define DEVICE_ADDRESS "0100000A"
// #define NETWORK_SESSION_KEY "2B7E151628AED2A6ABF7158809CF4F3C"
// #define APP_SESSION_KEY "A386262DEA626262EAAA636360099DEA"

const char *devAddr = DEVICE_ADDRESS;
const char *nwkSKey = NETWORK_SESSION_KEY;
const char *appSKey = APP_SESSION_KEY;

// unconnected pin used to generate pseudo random number seed
#define unconnectedAnalogPin A0
// motion sensor sense circuit connected to a digital pin
#define PIR 8
// size of sent packet
#define PACKETSIZE 21
// how much time to wait before sending the start packet (milliseconds)
#define START_DELAY 5000L
// how much time to wait before turning back on
#define HYSTERESIS 250L

// each power-on event gets a unique UUID
int eventUUID[4];
// LoRaWAN packet sent flags
boolean startSent = false;
boolean finalSent = false;
// time last message sent
unsigned long downTime = 0L;
// chip select pin for the optional MicroSD Card Adapter
int chipSelect = 4;
// if true, the Micro SD card adapter initialized correctly
boolean okSD = false;

// Return a random bit.
// Uses 'casting out' to eliminate bias:
//   Von Neumann, J.,
//   "Various techniques used in connection with random digits",
//   Applied Mathematics Series, no. 12, 36-38 (1951).
unsigned int randomBit(void)
{
    unsigned long bit1 = 0;
    unsigned long bit0 = 0;
    unsigned long limit = 99;

    while ((0 != limit--) && (bit1 == bit0))
    {
        bit1 = analogRead(unconnectedAnalogPin) & 1;
        delay(1);
        bit0 = analogRead(unconnectedAnalogPin) & 1;
        delay(1);
    }
    return (bit1);
}

// Return 'noOfBits' random bits.
unsigned long createRandomSeed(unsigned int noOfBits)
{
    unsigned long rand = 0;
    for (int i = 0; i < noOfBits; i++)
        rand = (rand << 1) | randomBit();
    return (rand);
}

// Seed random number generator with some arbitrary value.
void seedRandomNumberGenerator()
{
    unsigned long seed = createRandomSeed(31);
    randomSeed(seed);
}

// Generate a universally unique identifier (UUID). 
void generatedUUID(int uuid[4])
{
    int i;
    byte randomBytes[16];
    int number;

    // initialize random number generator
    seedRandomNumberGenerator();

    // get 16 random bytes
    for (i = 0; i < 16; i++)
        randomBytes[i] = random(256);

    randomBytes[6] = (byte)(randomBytes[6] & 15);
    randomBytes[6] = (byte)(randomBytes[6] | 64);
    randomBytes[8] = (byte)(randomBytes[8] & 63);
    randomBytes[8] = (byte)(randomBytes[8] | 128);

    // compute two 'longs' as four int
    number = 0;
    for(i = 0; i < 4; ++i) {
        number = number << 8 | (long)(randomBytes[i] & 0xff);
    }
    uuid[0] = number;
    number = 0;
    for(i = 4; i < 8; ++i) {
        number = number << 8 | (long)(randomBytes[i] & 0xff);
    }
    uuid[1] = number;
    number = 0;
    for(i = 8; i < 12; ++i) {
        number = number << 8 | (long)(randomBytes[i] & 0xff);
    }
    uuid[2] = number;
    number = 0;
    for(i = 12; i < 16; ++i) {
        number = number << 8 | (long)(randomBytes[i] & 0xff);
    }
    uuid[3] = number;
}

void createPacket(byte bytes[PACKETSIZE])
{
    unsigned long time;
    int state;

    // send the event UUID, mostSigBits then leastSigBits
    // when considered as two longs, this can be reconstructed into a string at the server with Java code like:
    //
    // String digits(long val, int digits) {
    //     long hi = 1L << (digits * 4);
    //     return Long.toHexString(hi | (val & (hi - 1))).substring(1);
    // }
    //
    // String toString() {
    //     return (digits(mostSigBits >> 32, 8) + "-" +
    //             digits(mostSigBits >> 16, 4) + "-" +
    //             digits(mostSigBits, 4) + "-" +
    //             digits(leastSigBits >> 48, 4) + "-" +
    //             digits(leastSigBits, 12));
    // }
    bytes[0] = (eventUUID[0] >> 24) & 0xff;
    bytes[1] = (eventUUID[0] >> 16) & 0xff;
    bytes[2] = (eventUUID[0] >> 8) & 0xff;
    bytes[3] = eventUUID[0] & 0xff;
    bytes[4] = (eventUUID[1] >> 24) & 0xff;
    bytes[5] = (eventUUID[1] >> 16) & 0xff;
    bytes[6] = (eventUUID[1] >> 8) & 0xff;
    bytes[7] = eventUUID[1] & 0xff;
    bytes[8] = (eventUUID[2] >> 24) & 0xff;
    bytes[9] = (eventUUID[2] >> 16) & 0xff;
    bytes[10] = (eventUUID[2] >> 8) & 0xff;
    bytes[11] = eventUUID[2] & 0xff;
    bytes[12] = (eventUUID[3] >> 24) & 0xff;
    bytes[13] = (eventUUID[3] >> 16) & 0xff;
    bytes[14] = (eventUUID[3] >> 8) & 0xff;
    bytes[15] = eventUUID[3] & 0xff;

    // send the system clock
    time = millis();
    bytes[16] = (time >> 24) & 0xff;
    bytes[17] = (time >> 16) & 0xff;
    bytes[18] = (time >> 8) & 0xff;
    bytes[19] = time & 0xff;

    // send the motion sensor state
    state = digitalRead(PIR);
    if (state == HIGH) // if the sensor is off
        bytes[20] = 0;
    else
        bytes[20] = 1;
}

void queuePacket()
{
    int i;
    byte bytes[PACKETSIZE];

    createPacket(bytes);
    LoRaWAN.beginPacket();
    for(i = 0; i < 22; i++)
        LoRaWAN.write(bytes[i]);
    LoRaWAN.endPacket();
}


void sendPacket()
{
    int i;
    byte bytes[PACKETSIZE];

    createPacket(bytes);
    LoRaWAN.sendPacket(bytes, PACKETSIZE, false);
}

char toHex(byte b)
{
    char ret;
    if (b < 10)
       ret = b + 0x30; // '0'
    else if (b < 16)
       ret = (b - 10) + 0x61; // 'a'
    else
       ret = 'x';
    return (ret);
}

void writeSD()
{
    unsigned long time;
    int state;
    File file; // file object that is used to read and write data

    if (okSD) {
        file = SD.open("HONEYPOT.LOG", FILE_WRITE); // open file to write data
        if (file) {
            // generate a log line
            char line[32+4+1+8+1+1+1]="ae2d0dd7-6273-43d2-b439-1a35ab5795e8,ffffffff,0";

            // write the event UUID
            line[0] = toHex((eventUUID[0] >> 28) & 0xf);
            line[1] = toHex((eventUUID[0] >> 24) & 0xf);
            line[2] = toHex((eventUUID[0] >> 20) & 0xf);
            line[3] = toHex((eventUUID[0] >> 16) & 0xf);
            line[4] = toHex((eventUUID[0] >> 12) & 0xf);
            line[5] = toHex((eventUUID[0] >>  8) & 0xf);
            line[6] = toHex((eventUUID[0] >>  4) & 0xf);
            line[7] = toHex((eventUUID[0] >>  0) & 0xf);

            line[9] = toHex((eventUUID[1] >> 28) & 0xf);
            line[10] = toHex((eventUUID[1] >> 24) & 0xf);
            line[11] = toHex((eventUUID[1] >> 20) & 0xf);
            line[12] = toHex((eventUUID[1] >> 16) & 0xf);

            line[14] = toHex((eventUUID[1] >> 12) & 0xf);
            line[15] = toHex((eventUUID[1] >>  8) & 0xf);
            line[16] = toHex((eventUUID[1] >>  4) & 0xf);
            line[17] = toHex((eventUUID[1] >>  0) & 0xf);

            line[19] = toHex((eventUUID[2] >> 28) & 0xf);
            line[20] = toHex((eventUUID[2] >> 24) & 0xf);
            line[21] = toHex((eventUUID[2] >> 20) & 0xf);
            line[22] = toHex((eventUUID[2] >> 16) & 0xf);

            line[24] = toHex((eventUUID[2] >> 12) & 0xf);
            line[25] = toHex((eventUUID[2] >>  8) & 0xf);
            line[26] = toHex((eventUUID[2] >>  4) & 0xf);
            line[27] = toHex((eventUUID[2] >>  0) & 0xf);
            line[28] = toHex((eventUUID[3] >> 28) & 0xf);
            line[29] = toHex((eventUUID[3] >> 24) & 0xf);
            line[30] = toHex((eventUUID[3] >> 20) & 0xf);
            line[31] = toHex((eventUUID[3] >> 16) & 0xf);
            line[32] = toHex((eventUUID[3] >> 12) & 0xf);
            line[33] = toHex((eventUUID[3] >>  8) & 0xf);
            line[34] = toHex((eventUUID[3] >>  4) & 0xf);
            line[35] = toHex((eventUUID[3] >>  0) & 0xf);

            // write the system clock
            time = millis();
            line[37] = toHex((time >> 28) & 0xf);
            line[38] = toHex((time >> 24) & 0xf);
            line[39] = toHex((time >> 20) & 0xf);
            line[40] = toHex((time >> 16) & 0xf);
            line[41] = toHex((time >> 12) & 0xf);
            line[42] = toHex((time >>  8) & 0xf);
            line[43] = toHex((time >>  4) & 0xf);
            line[44] = toHex((time >>  0) & 0xf);

            // write the motion sensor state
            state = digitalRead(PIR);
            if (state == HIGH) // if the sensor is off
                line[46] = '0';
            else
                line[46] = '1';
            line[47] = 0;

            file.println(line); // write message to file
            file.close(); // close file
        }
        else
            okSD = false;
    }
}

void setup( void )
{
#if defined(REGION_AS923_920_923)
    LoRaWAN.begin(AS923);
    LoRaWAN.addChannel(2, 922200000, 0, 5);
    LoRaWAN.addChannel(3, 922400000, 0, 5);
    LoRaWAN.addChannel(4, 922600000, 0, 5);
    LoRaWAN.addChannel(5, 922800000, 0, 5);
    LoRaWAN.addChannel(6, 923000000, 0, 5);
    LoRaWAN.addChannel(7, 922000000, 0, 5);
    LoRaWAN.addChannel(8, 922100000, 6, 6);
    LoRaWAN.addChannel(9, 921800000, 7, 7);
#endif

#if defined(REGION_AS923_920_923)
    LoRaWAN.begin(AS923);
    LoRaWAN.addChannel(2, 923600000, 0, 5);
    LoRaWAN.addChannel(3, 923800000, 0, 5);
    LoRaWAN.addChannel(4, 924000000, 0, 5);
    LoRaWAN.addChannel(5, 924200000, 0, 5);
    LoRaWAN.addChannel(6, 924400000, 0, 5);
    LoRaWAN.addChannel(7, 924600000, 0, 5);
    LoRaWAN.addChannel(8, 924500000, 6, 6);
    LoRaWAN.addChannel(9, 924800000, 7, 7);
#endif

#if defined(REGION_AU915)
    LoRaWAN.begin(AU915);
    LoRaWAN.setSubBand(2);
#endif

#if defined(REGION_EU868)
    LoRaWAN.begin(EU868);
    LoRaWAN.addChannel(1, 868300000, 0, 6);
    LoRaWAN.addChannel(3, 867100000, 0, 5);
    LoRaWAN.addChannel(4, 867300000, 0, 5);
    LoRaWAN.addChannel(5, 867500000, 0, 5);
    LoRaWAN.addChannel(6, 867700000, 0, 5);
    LoRaWAN.addChannel(7, 867900000, 0, 5);
    LoRaWAN.addChannel(8, 868800000, 7, 7);
    LoRaWAN.setRX2Channel(869525000, 3);
#endif

#if defined(REGION_IN865)
    LoRaWAN.begin(IN865);
#endif

#if defined(REGION_KR920)
    LoRaWAN.begin(KR920);
    LoRaWAN.addChannel(3, 922700000, 0, 5);
    LoRaWAN.addChannel(4, 922900000, 0, 5);
    LoRaWAN.addChannel(5, 923100000, 0, 5);
    LoRaWAN.addChannel(6, 923300000, 0, 5);
#endif

#if defined(REGION_US915)
    LoRaWAN.begin(US915);
    LoRaWAN.setSubBand(2);
#endif

    LoRaWAN.setDutyCycle(false);
    LoRaWAN.setSaveSession(true);
    // LoRaWAN.setAntennaGain(2.0);
    LoRaWAN.joinABP(devAddr, nwkSKey, appSKey);

    generatedUUID(eventUUID);
    pinMode(LED_BUILTIN, OUTPUT);
    pinMode(PIR, INPUT);
    pinMode(chipSelect, OUTPUT); // chip select pin must be set to OUTPUT mode
    if (SD.begin(chipSelect)) { // Initialize SD card
       okSD = true;
    }
    digitalWrite(LED_BUILTIN, 0);
}

void loop( void )
{
    unsigned long time;
    int state;
    unsigned long diff;

    if (LoRaWAN.joined() && !LoRaWAN.busy())
    {
        if (!finalSent)
            digitalWrite(LED_BUILTIN, 1);

        time = millis();
        if (!startSent)
        {
            if (time >= START_DELAY) // probably not needed because it takes time to join
            {
                sendPacket();
                writeSD();
                startSent = true;
            }
        }
        else
        {
            state = digitalRead(PIR);
            if (!finalSent)
            {
                if (state == HIGH) // if the sensor is off
                {
                    digitalWrite(LED_BUILTIN, 0);
                    finalSent = true;
                    downTime = time;
                    sendPacket();
                    writeSD();
                }
            }
            else
            {
                diff = time - downTime;
                if ((state == LOW) && (diff > HYSTERESIS)) // if the sensor is back on after long enough then restart
                {
                    startSent = false;
                    finalSent = false;
                    downTime = 0L;
                }
            }
        }
    }
}
