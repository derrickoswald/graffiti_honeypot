# Prototype Graffiti Honeypot

## Step 1: Passive Infra-Red (PIR) Sensor Circuit

### Parts List

| Description      | Part Number and Datasheet |
| ----------- | ----------- |
| solderless breadboard      |        |
| miscellaneous hookup wires      |        |
| 9V battery      | Alkaline (6LP3146) 550mAh       |
| 9V battery connector   | 2 pin male/female snap connector        |
| PIR sensor | [555-28027](../datasheets/555-28027-PIR-Sensor-Prodcut-Doc-v2.2.pdf) |
| 5.6V Zener diode   | [BZX55C5V6](../datasheets/bzx55c-bs.pdf) |
| 1.1kΩ ¼W resistor | brown-brown-red color code |

### Design

#### Voltage Choice

Although other battery combinations could have been used,
a 9 Volt battery was chosen for simplicity and
to provide the Vin voltage (UNO: 7 - 12V, Nano: 6 - 12V) needed by
the linear regulator on an Arduino; given that there would be a small
voltage drop across the FET (see Step 2: [Load Switch Circuit](./Step%202.md)).
For example, a stack of 4×1.5V batteries in series may have 6 volts when new,
but this would not be enough for the UNO and the head-room on the Nano
would be very small - and would require four times as man parts.

A 9V battery is actually composed of 6 cells internally,
at about 1.58 Volts each,
leading to a voltage of 9.48 Volts when new.
This degrades over use.
The standard Alkaline 6LP3146 battery can provide about 600 mAh
(i.e. 6 milliAmps for 100 hours), but the specification only says 550 mAh.

#### Supply to the PIR

The PIR sensor has a voltage input range of 3 to 6 Volts DC.
This is less than 9V obviously, so a voltage reduction is required.
Rather than build an efficient but complex switching regulator,
the simple but inefficient approach of a Zener diode was used.
The choice of Zener (3.3, 3.9, 4.7, 5.1, and 5.6 Volts for standard types)
is arbitrarily taken to be 5.6V

#### Resistor Choice

The PIR sensor onboard LED consumes about 3 mA when the circuit is on.
When off, the power consumption is negligible (130 μA idle × 5.6V).

The resistor size should be as large as possible to avoid wasted
power in idle mode, P = (9.0 - 5.6)/R.

It should also be small enough that the voltage drop at 3 mA
does not operate the Zener diode far from its breakdown voltage.
That is, at 3 mA, the voltage across R should be approximately 9 - 5.6 = 3.4 Volts,
so R = 3.4 / 0.003 = 1133Ω.
The nearest available resistor is 1.1kΩ.

### Construction

![Step 1 Circuit Diagram](../img/step1.svg "Step 1 Circuit Diagram")

Suggest the top rails + and - be used for the battery connection.

![Battery Polarity](../img/battery_connector.svg "Battery Polarity")

![Zener Polarity](../img/zener_polarity.svg "Zener Polarity")

![PIR Connector](../img/pir_connector.svg "PIR Connector")

### Test

Connect the battery.

The PIR Sensor requires a warm-up time in order to function properly. This is due to the settling time
involved in “learning” its environment. This could be up to 40 seconds. During this time, the LEDs under
the lens will be on and there should be as little motion as possible in the sensors field of view.

After the warm-up, wave a hand in front of the sensor and then pull your hand back behind the sensor.
The internal LEDs should light up while waving your hand.

Disconnect the battery to save its energy.

