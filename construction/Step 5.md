# Prototype Graffiti Honeypot

## Step 5: SD Card Circuit

### Parts List

| Description      | Part Number |
| ----------- | ----------- |
| 6pin microSD card reader module with level shifting chip | [HW-125](https://www.amazon.de/gp/product/B06Y662FB1)  |


### Construction

![Step 5 Circuit Diagram](../img/step5.svg "Step 5 Circuit Diagram")

Suggest using the breadboard bottom rail + for Arduino 5V and bottom rail - for Arduino Gnd.

### Test

Program the Arduino with the [Lora Sketch](../src/lora/lora.ino).

Insert a formatted MicroSD card into the reader.

Connect the battery.

The PIR sensor LED should turn on while it stabilizes.
When the circuit is again quiescent, wave your hand in front of the PIR for more than 5 seconds.
This should turn the sensor on, issue a LoraWAN message and write to a file on the card called HONEYPOT.LOG.

When the circuit is again quiescent, disconnect the battery to save its energy.

Extract the MicroSD card and examine the contents on another computer equiped with a SD card reader.

The contents of the HONEYPOT.LOG file should look something like this:

```
b3a7fe7e-7d3e-4824-a3b4-237f31507570,00001393,1
b3a7fe7e-7d3e-4824-a3b4-237f31507570,0000884f,0
bc023ce2-3287-4a4b-a7ce-f4a813fd2c98,0000138d,1
bc023ce2-3287-4a4b-a7ce-f4a813fd2c98,000032fa,0
```
This corresponds to the information in the following table:

|Unique Event ID|Uptime Clock (mS)|Sensor|
| ----------- | ----------- | ----------- |
|b3a7fe7e-7d3e-4824-a3b4-237f31507570|5011|on|
|b3a7fe7e-7d3e-4824-a3b4-237f31507570|34895|off|
|bc023ce2-3287-4a4b-a7ce-f4a813fd2c98|5005|on|
|bc023ce2-3287-4a4b-a7ce-f4a813fd2c98|13050|off|

These should correspond to the events received by the Things Network,
with a slight Uptime Clock offset due to the processing time on the device.
