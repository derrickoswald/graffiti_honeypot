# Prototype Graffiti Honeypot

## Step 3: Hysteresis Circuit

### Parts List

| Description      | Part Number and Datasheet |
| ----------- | ----------- |
| 100µF 16V capacitor | [16SEPC100M](../datasheets/16SEPC100M.pdf) |
| 47kΩ ¼W resistor | yello-violet-orange color code |

### Design

#### R-C Time Constant

As you probably observed, when the PIR sensor turns off the power to the Arduino is abruptly cut.
For some quick motions, the Arduino is turned on and off too fast for the sketch to run,
that is, it doesn't even get one blink in time. 
What would be better, is if the power to the Arduino could be held on for a while,
so the minimum 'on' time is a few seconds -
and later on we'll add a circuit to let the Arduino know the power is going off soon.

The FET is a gate voltage driven device, so all that is required is to keep the
voltage high enough for long enough.
We'll use a Resistance-Capacitance circuit for this.
When the PIR sensor turns on, a capacitor is charged up,
and when it turns off, the capacitor discharges through the resistor.
The rate of discharge (the time constant) is set by
the product of capacitance and resistance.

Since the FET gate threshold voltage is between 1 and 2 volts,
and the output high voltage of the sensor is ~5.6 Volts,
the percentage difference = (5.6 - 2.0) / 5.6 × 100 ~= 63%
which is one R-C time constant.

If we use a 100µF capacitor, and we want the 'on time' to be about 5 seconds,
the appropriate resistance is R = 5.0 / 100e-6 = 50kΩ,
so we choose a similar standard resistor of 47kΩ.

### Construction

![Step 3 Circuit Diagram](../img/step3.svg "Step 3 Circuit Diagram")

Observe the polarity of the capacitor.

![Capacitor Polarity](../img/cap_pinout.svg "Capacitor Polarity")

### Test

Connect the battery.

Waving your hand in front of the PIR sensor should now turn on the
Arduino for about 3 blinks, no matter how short the wave.

Disconnect the battery to save its energy.