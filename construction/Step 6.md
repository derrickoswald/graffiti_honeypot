# Prototype Graffiti Honeypot

## Step 6: Camera Module Circuit

### Parts List

| Description      | Part Number |
| ----------- | ----------- |
| ArduCAM OV5642 Camera Shield | [B0068](https://www.arducam.com/product/arducam-5mp-plus-spi-cam-arduino-ov5642/)  |

Note: The above part includes Dupont connector cable.

### Construction

![Step 6 Circuit Diagram](../img/step6.svg "Step 6 Circuit Diagram")

### Test

_TBD_
