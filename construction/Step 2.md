# Prototype Graffiti Honeypot

## Step 2: Load Switch Circuit

### Parts List

| Description      | Part Number and Datasheet |
| ----------- | ----------- |
| FET | [IRLZ24N](../datasheets/IRLZ24N.pdf)  |
| LED | green |
| 750Ω ¼W resistor | violet-green-brown color code |

### Design

#### FET

The chosen FET is admittedly over-kill for what is needed here.
The blocking voltage (55 Volts),
continuous drain current (18 Amps),
and on resistance (0.06 Ohms),
are all excessive for this application,
but it was available in personal stock.
This part is now obsolete, being replaced perhaps by
[IRLZ24NPBF](https://www.infineon.com/dgdl/irlz24npbf.pdf?fileId=5546d462533600a401535671ff87271e)
with equivalent specifications and price.
It's nice to have this kind of tool in your belt,
as it can be applied in most hobby uses (~80¢ each).

### Dummy Load Circuit

One might be tempted to just attach an Arduino when the FET is installed,
but from experience, it is better to have a dummy load to test with.
For this purpose an LED and resistor can be used.
The resistor should be sized to drop the 9 Volts of the battery,
while allowing 10 mA or so to supply the LED.  
Since a forward biased LED has approximately 1.5 volts,
the appropriate resistance is R = (9.0 - 1.5) / 0.010 = 750 Ω.

### Construction

![Step 2 Circuit Diagram](../img/step2.svg "Step 2 Circuit Diagram")

Suggest using the bottom rail - for Arduino ground
(eventually, +5 Volts from the Arduino can use the bottom rail +).

![FET Pinout](../img/fet_pinout.svg "FET Pinout")

![LED Pinout](../img/led_pinout.svg "LED Pinout")

### Test

With the dummy load connected, connect the battery.

Waving your hand should illuminate the LED.

Disconnect the battery.

Program the Arduino with the [Blink Sketch](../src/blink/blink.ino).

Disconnect the dummy load.

Connect the Arduino in its place
being careful to observe the correct polarity -
B+ to the Arduino Vin and the FET Source pin to Arduino GND.

Connect the battery.

Waving your hand should blink the Arduino LED.

Disconnect the battery to save its energy.
