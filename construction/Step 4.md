# Prototype Graffiti Honeypot

## Step 4: Sense Circuit

### Parts List

| Description      | Part Number and Datasheet |
| ----------- | ----------- |
| Diode | [IN4007](../datasheets/1N4001-1N4007(DO-41).pdf)  |
| FET | [IRLZ24N](../datasheets/IRLZ24N.pdf)  |
| 2 × 845kΩ ¼W resistor | grey-yello-green-orange color code |

### Design

#### PIR Output Equivalent Circuit

The output stage of the Passive Infra-Red (PIR) sensor has
an active pull-up (transistor or FET)
but only a 1 MΩ pull down resistor.

![PIR Output Stage](../img/pir_output.svg "PIR Output Stage")

Also, relative to the Arduino Gnd pin,
the voltage of the output goes below zero - 
which isn't good for the Arduino Digital I/O pins.

For this reason, we add a FET (yes, another one) to
perform the voltage shift and handle the weak pull-down
from of the PIR output.

The FET drain is connected to Arduino Gnd,
and the FET source is connected to Arduino 5V
through a pull-up resistor.

#### Diode

Since we have a hysterisis circuit (from [Step 3](./Step%203.md))
the true state of the PIR output is no longer available -
it would have whatever voltage is remaining on the capacitor.

So, to decouple the PIR input from the hysterisis circuit
we add a diode. Most any diode would do,
but what was available in personal stock
are [1N4007](../datasheets/1N4001-1N4007(DO-41).pdf) (1000V 1A),
which is overkill for this little application,
but it's nice to have this kind of tool in your belt,
as it can be applied in most hobby uses (~11¢ each).

The gate of the FET is attached directly to the PIR output,
on the anode side of the diode.

#### Current Limiting Resistor

One could connect the Digital I/O pin directly to the FET drain,
but if the Digital I/O pin is ever incorrectly programmed
as an output with logic 1 output (high),
and the PIR sensor is on - turning the FET on - there would
be a glorious battle between the FET and the Digital I/O pin,
with the foregone result that the Digital I/O pin would lose - badly.

So we add a resistance in series with the input pin to
limit the current in such an erroneous case.

Since this is a sensing cicuit the pull-up resistor and
the current limiting resistor can be quite high values, e.g. 1MΩ,
but what is in stock is 845kΩ.

### Construction

![Step 4 Circuit Diagram](../img/step4.svg "Step 4 Circuit Diagram")

![Diode Polarity](../img/diode_polarity.svg "Diode Polarity")

![FET Pinout](../img/fet_pinout.svg "FET Pinout")

Suggest using the bottom rail + for Arduino 5V.

### Test

Program the Arduino with the [Test Sketch](../src/test/test.ino).

Connect the battery.

The Arduino LED should be on constantly when the PIR sensor is on.
When the sensor is off, the Arduino LED will start to blink.
Blinking should stop after about 5 seconds when the Arduino loses power. 

Disconnect the battery to save its energy.