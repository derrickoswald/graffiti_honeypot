
### LoRaWan Hub

[Miromico TBGW100 Hub](https://miromico.ch/wp-content/uploads/2019/11/Tabs_Hub_miro.pdf)

### LoRaWan Discovery Kit

[B-L072Z-LRWAN1 Discovery Kit Data Brief](https://www.st.com/resource/en/data_brief/b-l072z-lrwan1.pdf)  ~$46.50
embeds the CMWX1ZZABZ-091 LoRa®/Sigfox™ module (Murata) with an embedded ultra-low-power STM32L072CZ Series MCU,
based on Arm® Cortex®-M0+ core, with 192 Kbytes of Flash memory, 20 Kbytes of RAM, 20 Kbytes of EEPROM

[UM2115 User Manual](https://www.st.com/resource/en/user_manual/dm00329995-discovery-kit-for-lorawan-sigfox-and-lpwan-protocols-with-stm32l0-stmicroelectronics.pdf)

[UM2115 User Manual (local copy)](../datasheets/dm00329995-discovery-kit-for-lorawan-sigfox-and-lpwan-protocols-with-stm32l0-stmicroelectronics.pdf)

The board has connectors compatible with Arduino Uno (R3).

It can be powered by the same Vin as the Arduino
(7-12 V DC power supply plugged on Arduino™ Uno V3 connectors: VIN on pin 8 and GND on pin 7 of CN4)
The VIN and GND pins must be both connected.

Or it can be self powered by the 3xAAA-sized battery holder BT1 located on the bottom side of the Discovery kit.

EU-868 MHz

The clue: https://developer.helium.com/devices/arduino-quickstart/st-discovery-lrwan1
Helpful site: https://github.com/GrumpyOldPizza/ArduinoCore-stm32l0
installed at ~/.arduino15/packages/TleraCorp/hardware/stm32l0/0.0.10/drivers/linux
2ni version https://github.com/2ni/lorawan_modem

### LoRaWan Module

[FMLR-6x-x-RSxx-(4M)](https://miromico.ch/portfolio/fmlr_renesas/) ~€13,92


[Datasheet](https://miromico.ch/wp-content/uploads/2019/02/FMLR_61_x_RSS3.pdf)

![FMLR-6x-x-RSxx-(4M)](https://miromico.ch/wp-content/uploads/2018/12/FMLR_Renesas-1.png "FMLR-6x-x-RSxx-(4M)")

Not supplied:

[Antenna (868Mhz, U.FL connector)](https://www.digikey.com/en/products/detail/ethertronics-avx/1001826/4754419) ~$3.98 [Datasheet](https://media.digikey.com/pdf/Data%20Sheets/Ethertronics/1001826_May2013.pdf)

![Flat Bar Antenna](https://media.digikey.com/photos/Ethertronics%20Photos/1001826.jpg "Flat Bar type antenna with U.FL connector")

